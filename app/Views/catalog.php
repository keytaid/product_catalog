<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
  <title><?php echo isset($shop)?$shop->name."- Product Catalog":"Product Catalog" ?></title>
  <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
    
    input { 
        text-align: center; 
    }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
</head>
<body>
<main>

<div class="album py-5 bg-light">
  <div class="container">
  <form method="post" action="">
    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">

      <?php foreach($products as $product): ?>

      <div class="col">
        <div class="card shadow-sm">
          <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>

          <div class="card-body">
            <h6><?php echo $product->name ?></h6>
            <p class="card-text">This is product description.</p>
            <h6><?php echo showPrice($product->price, $product->discount) ?></h6>
            <div class="d-flex justify-content-between align-items-center">
              <small class="text-muted"></small>
              <div class="quantity btn-group w-50">
                <input type="number" name="quantity" class="form-control input w-50" min="0" value="0"/>
              </div>
            </div>
          </div>
        </div>
      </div>

      <?php endforeach; ?>

    </div>
  </form>
  </div>
</div>

</main>
</body>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
<script>
$(function() {
    $(".quantity").prepend('<button type="button" class="btn btn-sm btn-outline-secondary"> - </button>');
    $(".quantity").append('<button type="button" class="btn btn-sm btn-outline-secondary"> + </button>');

    $(".btn").on("click", function() {

        var $button = $(this);
        var oldValue = $button.parent().find("input").val();

        if ($button.text() == " + ") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
        // Don't allow decrementing below zero
        if (oldValue > 0) {
            var newVal = parseFloat(oldValue) - 1;
        } else {
            newVal = 0;
        }
        }

        $button.parent().find("input").val(newVal);

    });
});


</script>
</html>  