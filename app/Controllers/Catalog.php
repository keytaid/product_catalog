<?php namespace App\Controllers;

use Firebase\JWT\JWT;
class Catalog extends BaseController
{
	public function index()
	{
		echo "shop";
	}

	public function shop($id)
	{
		helper('util');
		if (!isset($_ENV['SERVER_JWT_KEY']) || !isset($_ENV['SERVER_URL'])){
			
			return;
		}
		$jwt = new JWT();
		$payload = array(
			'user_id' => 2,
			'exp' => strtotime("+1 hours")
		);
		$token = $jwt->encode($payload, $_ENV['SERVER_JWT_KEY']);
		
		$options = array('http' => array(
			'method'  => 'GET',
			'header' => 'Authorization: Bearer '.$token
		));
		$context  = stream_context_create($options);
		$products = json_decode(
			file_get_contents($_ENV['SERVER_URL'].'products?q[shop_id_eq]='.$id, false, $context)
		)->results;
		
		$data['products'] = $products;
		if (!empty($products)){
			$data['shop'] = $products[0]->shop;
		}

		return view('catalog', $data);
	}
}
