<?php

function toRupiah($string){
    setlocale(LC_MONETARY,"in_ID");
    return "Rp ".number_format($string, 0, ',', '.');
}

function showPrice($price, $discount){
    if ($discount > 0){
        $priceString = '<span style="text-decoration-line: line-through;">'."Rp ".number_format($price, 0, ',', '.').'</span>';
        if ($discount <= 100){
            $price = $price - (($price * $discount) / 100);
            $discountString = "Rp ".number_format($price, 0, ',', '.');
        } else {
            $price = $price - $discount;
            $discountString = "Rp ".number_format($price, 0, ',', '.');
        }
        return $priceString." ".$discountString;
    } else {
        return "Rp ".number_format($price, 0, ',', '.');
    }
}